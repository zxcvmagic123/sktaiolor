import React, { Component } from 'react';
import {
    StyleSheet,Text,View,Button,TextInput
  } from 'react-native';

  import * as firebase from "firebase/app";
  import 'firebase/database';
  // import 'firebase/auth'
  import config from '../firebase/config'
  
  

export default class Home extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          email: 'Email',
          password: 'Password',
          currentUser: null,
          message: '',
          OrderList:{}
        }
    }
    componentDidMount(){
      

      // if (!firebase.apps.length) {
      //   firebase.initializeApp(config)
      // }
      // const database = firebase.database();
      //   //Collect Data as object
      //   const app = database.ref('users');
      //   app.on('value',snapshot => {
      //     // this.getData(snapshot.val());
      //     this.setState({
      //       userData : snapshot.val()
      //     })
      //     console.log(this.state.userData)
      //   });

     // console.log(this.props.route.params.currentUser.email)
      // if (this.props.route.params.currentUser.email) {
      //   this.setState({
      //     currentUser: this.props.route.params.currentUser.email
      //   })
      // }
    }
    catch(e){
        console.error(e);
      }

  //   logout = e => {
  //     console.log('Logout')
  //     console.log(this.currentUser)
  //       auth.signOut().then(response => {
  //         this.setState({
  //           currentUser: null
  //         })
  //         this.props.navigation.navigate('Login', { currentUser: this.currentUser })
  //       })   
  // }

    

      render(){
        const { navigate } = this.props.navigation
      
        return(
            <View>
              <Text>Home</Text>
                 {/* <Text>Hello {this.props.route.params.currentUser.email}</Text>
                 <Button title='Logout' onPress={this.logout}></Button> */}
                 <Button title='New Order' onPress={()=> navigate('CreateNewOrder')}></Button>
                 <Button title='View Order' onPress={()=>  navigate('ViewOrder')}></Button>
            </View>
          )
      }
}
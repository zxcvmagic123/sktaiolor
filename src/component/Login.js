import React, { Component } from 'react';
import {
  StyleSheet,Text,View,Button,TextInput
} from 'react-native';
// import { Button, Input } from 'react-native-elements';


//import firebase config
import auth from '../firebase';



export default class Login extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          email: 'test@example.com',
          password: '123456',
          currentUser: null,
          message: ''
        }
      }
    
      onChange = e => {
        const { name, value } = e.target
    
        this.setState({
          [name]: value
        })
      }
      clearInput = e =>{
        this.setState({email:'',password:''})
      }
    
      onSubmit = e => {
        e.preventDefault()
    
        const { email, password } = this.state
        
        // TODO: implement signInWithEmailAndPassword()
        auth
        .signInWithEmailAndPassword(email, password)
        .then(response => {
          this.setState({
            currentUser: response.user
          })
          console.log(currentUser)
        })
        .catch(error => {
          this.setState({
            message: error.message
          })
        })           
      }

      // logout = e => {
      //   navigate('Login', { currentUser: this.currentUser })
      //   e.preventDefault()
      //   auth.signOut().then(response => {
      //     this.setState({
      //       currentUser: null
      //     })
      //   })
      // }

    componentDidMount() {
      console.log("---- will mount -----")
      auth.onAuthStateChanged(user => {
        if (user) {
          this.setState({
            currentUser: user
          })
        }
      })

      
    }
      catch(e){
        console.error(e);
      }
      
    
    
 
 render() {
  const { message, currentUser } = this.state
  const { navigate } = this.props.navigation
  
  //After Loginpass
  if (currentUser) {
    navigate('Home', { currentUser: currentUser })
    // return (
    //   <View>
    //     <Text>Hello {currentUser.email}</Text>
    //     <Button title='Logout' onPress={this.logout}></Button>
    //   </View>
    // )
  }

    return (
      
      <View styles={styles.container} >

          <TextInput placeholder= {'Email'} styles={styles.Input} value={this.state.email} onChangeText={(email)=>this.setState({email})} />
          <TextInput placeholder= {"Password"} styles={styles.Input} value={this.state.password} secureTextEntry={true} onChangeText={(password)=>this.setState({password})}/>
          
          {message ? <Text>{message}</Text> : null}
          <Button title="Summit" onPress={this.onSubmit}  />
          <Button title="Cancel" onPress={this.clearInput}/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  Input:{
    height: 40, borderColor: 'gray', borderWidth: 1
  } 
});
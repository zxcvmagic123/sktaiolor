import React, { Component } from 'react';
import {
    StyleSheet,Text,View,Button,TextInput
  } from 'react-native';
import auth from '../firebase';

export default class CreateNewOrder extends React.Component {
    constructor(props){
        super(props);
        this.state = {
          OrderList:{
            Name:'',
            Detail:[{
              Type:'',
              Price:'',
              MaterailCode:''
            }],
            TotalPrice:0,
            PaymentMethod:'',
            PaymentType:'',
            TrancationNo:''
          }

        }
    } 

    onChange(Input,OrderList){
      this.setState({OrderList:{...OrderList,Name:Input}})
    }
    onSubmit =e=>{
      e.preventDefault()
      console.log(this.state.OrderList)

    }

      render(){
        const { navigate } = this.props.navigation
        return(
            <View>
                 <Text>CreateNewOrder</Text>
                 <TextInput placeholder= {'Name'} 
                 styles={styles.Input} 
                 value={this.state.Name} 
                 onChangeText={
                   (Input) =>this.onChange(Input,this.state.OrderList) } />
                   
                   <Text>{'\n'}{'\n'}</Text>

                 <Button title="Add Order" 
                        onPress={()=> navigate('OrderDetail',{OrderList:this.state.OrderList})}/>
                 <Button title="Submit" onPress={this.onSubmit}  />
            </View>
          )
      }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF'
  },
  Input:{
    height: 40, borderColor: 'gray', borderWidth: 1
  } 
});
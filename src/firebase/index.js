import firebase from 'firebase/app'
import 'firebase/auth'
import config from './config'

if (!firebase.apps.length) {
  firebase.initializeApp(config)
}

console.log('firebase')
export default firebase.auth()

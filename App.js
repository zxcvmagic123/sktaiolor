import React , { Component } from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

//import screens
import LoginScreen from './src/component/Login'
import HomeScreen from './src/component/Home'
import OrderDetailScreen  from './src/component/OrderDetailnew'
import CreateNewOrderScreen from './src/component/CreateNewOrder'
import ViewOrderScreen from './src/component/ViewOrder'

const Stack = createStackNavigator();

class App extends Component {


    render() {
        return (
            <View style={{flex:1,}}>
                <NavigationContainer>
                    <Stack.Navigator screenOptions={{headerShown:false}}>
                        {/* <Stack.Screen name="Login" component={LoginScreen}/> */}
                        <Stack.Screen name="Home" component={HomeScreen} />
                        <Stack.Screen name="OrderDetail" component={OrderDetailScreen} />
                        <Stack.Screen name="CreateNewOrder" component={CreateNewOrderScreen} />
                        <Stack.Screen name="ViewOrder" component={ViewOrderScreen} />
                    </Stack.Navigator>
                </NavigationContainer>
            </View>
                    )
    }
}

export default App;